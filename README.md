# Video conference buttons

I got a Raspberry Pico (RP2040) and decided as I spent most of my working hours from home in some of that time in meetings to make a little project that can mute audio/video and raise your hand with a couple of buttons

Luckily AdaFruit has done most of the work with their CircuitPython project with has an [^1] adafruit_hid library that allows the pico to function as a hid "Human Interface Adapter", a.k.a a keyboard.

They also have an [^5] tutorial that builds a custom keyboard with pico, including designing a PCB, so all I had to do is write some code based on that tutorial and send the proper keycodes over the USB connection. and ofcourse design the PCB and Case.

# Schematic

The schematic and PCB are made with [^2] KiCad and manufactured by [^4] Aisler

The board gets it's power from the USB plug on the pico board (this is not visible in the schematic)

All the switches are hooked up to GPIO pin's configured as digital inputs with a pull up resistor, the SSD1306 display uses I2C

As there are 2 different versions of the SSD1306 out there where the GND and VCC pins are switched you can use both by soldering the left or right sides of the jumpers

![Schematic](./images/schematic.png)
# PCB Board

All signal lines are on the top layer, while the bottom layer is filled with GND 

![PCB board](./images/pcb_aisler.png)

The board at Aisler: https://aisler.net/p/HXAFRBPY

# Case

The case consist of lasercut bottom and top Acylaat layers, with the sides 3d printed and the PCB in the middle

The case is designed in [^3] Fusion360 with besides of being an excellent design tool. gives you exploding view animations and high quality renderings with a few clicks. 

![Rendering](./images/case_rendering.png)

![Exploded View](./images/case_exploded_view.gif)

The 3D models for the electronic parts are courtesy of [^6] GrabCad with has an extensive library and community for almost any part you can think off. 


# Installing the software

  - Follow the [instructions here](https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython/circuitpython) to install CircuitPython 7.x on the Pico
  - plug it in with a USB cable, you will see a CIRCUITPY network drive appear
  - copy over the contents of the src/ directory to the drive

# Known Issues

 - Microsoft Teams
   - Keyboard shortcuts are different between Windows and Mac's, if you are on Windows please uncomment the windows keymap_teams definition in [main.py](src/main.py#L46) (and comment out the mac one) on the netwerk drive you see when plugging it in.
   - On the Mac mute/unmute video makes the teams app lose focus, so you have to click it again to get the focus back

# References

[^1]: [AdaFruit HID Library](https://docs.circuitpython.org/projects/hid/en/latest/)
[^2]: [Kicad](https://www.kicad.org/)
[^3]: [Fusion360](https://www.autodesk.com/products/fusion-360/personal)
[^4]: [Aisler](https://aisler.net/)
[^5]: [AdaFruit Keyboard Project](https://learn.adafruit.com/diy-pico-mechanical-keyboard-with-fritzing-circuitpython/overview)
[^6]: [Grabcad](https://grabcad.com/library)
