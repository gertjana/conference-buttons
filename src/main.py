import time
import board
from digitalio import DigitalInOut, Direction, Pull

import usb_hid
from lib.adafruit_hid.keyboard import Keyboard
from lib.adafruit_hid.keycode import Keycode
from lib.adafruit_hid.consumer_control import ConsumerControl

import busio
import displayio
import adafruit_displayio_ssd1306
from adafruit_display_text import label
from adafruit_bitmap_font import bitmap_font
from displayio import Bitmap

displayio.release_displays()

i2c = busio.I2C(board.GP21, board.GP20)
display_bus = displayio.I2CDisplay(i2c, device_address=0x3C)
display = adafruit_displayio_ssd1306.SSD1306(display_bus, width=128, height=64)

font = bitmap_font.load_font("fonts/roboto-20.bdf", Bitmap)

keymap_label = "not_defined"

led = DigitalInOut(board.LED)
led.direction = Direction.OUTPUT
led.value = True

kbd = Keyboard(usb_hid.devices)
cc = ConsumerControl(usb_hid.devices)

pins_buttons = [board.GP0, board.GP1, board.GP2]
pins_switch = [board.GP13, board.GP14, board.GP15]

keymap = {}

# for MacOS
keymap_teams = {
    (0): (Keycode.GUI, Keycode.SHIFT, Keycode.M),
    (1): (Keycode.GUI, Keycode.SHIFT, Keycode.O),
    (2): (Keycode.GUI, Keycode.SHIFT, Keycode.K),
}

# for windows
# keymap_teams = {
#     (0): (Keycode.CONTROL, Keycode.SHIFT, Keycode.M),
#     (1): (Keycode.CONTROL, Keycode.SHIFT, Keycode.O),
#     (2): (Keycode.CONTROL, Keycode.SHIFT, Keycode.K),
# }


keymap_zoom = { 
    (0): (Keycode.GUI, Keycode.SHIFT, Keycode.A),
    (1): (Keycode.GUI, Keycode.SHIFT, Keycode.V),
    (2): (Keycode.ALT, Keycode.Y),
}

keymap_hangouts = {
    (0): (Keycode.GUI, Keycode.D),
    (1): (Keycode.GUI, Keycode.E),
    (2): (Keycode.GUI, Keycode.CONTROL, Keycode.H),
}

switch = [0, 1, 2]
for s in range(3):
    switch[s] = DigitalInOut(pins_switch[s])
    switch[s].direction = Direction.INPUT
    switch[s].pull = Pull.UP

buttons = [0, 1, 2]
for i in range(3):
    buttons[i] = DigitalInOut(pins_buttons[i])
    buttons[i].direction = Direction.INPUT
    buttons[i].pull = Pull.UP

button_state = [0, 0, 0]

def show_label(group, txt, x, y):
    lbl = label.Label(font, text=txt)
    lbl.x = x
    lbl.y = y
    group.append(lbl)

def show_icon(group, path, x, y):
    bitmap = displayio.OnDiskBitmap(path)
    tile_grid = displayio.TileGrid(bitmap, pixel_shader=bitmap.pixel_shader)
    tile_grid.x = x
    tile_grid.y = y
    group.append(tile_grid)

def update_display():
    group = displayio.Group()

    show_label(group, keymap_label, 10, 10)

    if button_state[0] == 1:
        show_icon(group, "/icons/audio.bmp", 10, 26)

    if button_state[1] == 1:
        show_icon(group, "/icons/video.bmp", 48, 26)

    if button_state[2] == 1:
        show_icon(group, "/icons/raise_hand.bmp", 86, 26)

    display.show(group)
    
def show_logo():
    group = displayio.Group()
    show_icon(group, "/icons/logo.bmp", 0,0)
    display.show(group)
    
    
def get_keymap():
    if not switch[0].value:
        return keymap_teams, "MS Teams"
    if not switch[1].value:
        return keymap_zoom, "Zoom"
    if not switch[2].value:
        return keymap_hangouts, "Hangouts"

def check_button(button):
    if button_state[button] == 0:
        if not buttons[button].value:
            try:
                kbd.press(*keymap[button])
            except ValueError:  # deals w six key limit
                pass
            button_state[button] = 1

    if button_state[button] == 1:
        if buttons[button].value:
            try:
                kbd.release(*keymap[button])
            except ValueError:
                pass
            button_state[button] = 0

show_logo()
time.sleep(2)

while True:
    keymap, keymap_label = get_keymap()
    for button in range(3):
        check_button(button)
    update_display()
    time.sleep(0.01)  # debounce

